
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestAddition {

    String expectedArgument;
    StringCalculator ourCalculatorToTest = new StringCalculator();

    @Given("^a String argument containing the numbers to add$")
    public void a_String_argument_containing_the_numbers_to_add() throws Exception {
        String expectedArgument = new String();
    }

    @When("^the string argument is empty \"([^\"]*)\"$")
    public void the_string_argument_is_empty(String arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        expectedArgument = "";
        assertEquals(arg1,expectedArgument);
    }

    @Then("^the function returns \"([^\"]*)\"$")
    public void the_function_returns(String arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        int expectedResult = 0;
        assertEquals(ourCalculatorToTest.add(arg1),expectedResult);
    }

    @When("^the string argument contains only one number$")
    public void the_string_argument_contains_only_one_number(String arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String[] oneArg = arg1.split(",");
        assertEquals(oneArg.length, 1);
        try {
            Integer.parseInt(oneArg[0]);
        }catch (NumberFormatException e){
            System.out.println("This is not a number. " + arg1);
        }
    }

    @Then("^the function returns this number$")
    public void the_function_returns_this_number(String arg1) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        int expectedResult = Integer.parseInt(arg1);
        assertEquals(ourCalculatorToTest.add(arg1),expectedResult);
    }

    @When("^the string arguments contains two numbers$")
    public void the_string_arguments_contains_two_numbers(String stringReceived) throws Exception {
        expectedArgument = "1,2";
        String [] expectedSplitArgument = expectedArgument.split(",");
        String [] receivedSplitArgument = stringReceived.split(",");
        int [] expectedIntegers = new int[2];
        int [] receivedIntegers = new int[receivedSplitArgument.length];
        assertEquals(expectedIntegers.length,receivedIntegers.length);
        try {
            for (int i = 0; i < expectedSplitArgument.length; i++) {
                expectedIntegers[i] = Integer.parseInt(expectedSplitArgument[i]);
            }
            for (int i = 0; i < receivedSplitArgument.length; i++) {
                receivedIntegers[i] = Integer.parseInt(receivedSplitArgument[i]);
            }
        }catch (NumberFormatException e){
            System.out.println("The string received is not a number! " + stringReceived );
        }
    }

    @Then("^the function adds those two numbers and returns the addition$")
    public void the_function_adds_those_two_numbers_and_returns_the_addition(String stringReceived) throws Exception {

    }
}


