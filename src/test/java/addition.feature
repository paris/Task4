Feature: Addition

    Scenario: empty string addition
        Given a initial string argument
        When the string argument is empty ""
        Then the function returns "0"

    Scenario: one number addition
        Given a initial string argument
        When the string argument contains only one number
        Then the function returns this number

    Scenario: two number addition
        Given a initial string argument
        When the string arguments contains two numbers
        Then the function adds those two numbers and returns the addition




